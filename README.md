# Sport Data Valley client library for R

## Introduction
`sdvclient` is a R client library for the Sport Data Valley platform.
It is basically a wrapper around the REST API (documented [here](https://app.sportdatavalley.nl/api-docs/index.html)).

## Installation
If you are working in the Sport Data Valley JupyterHub environment, this library is automatically installed.
Otherwise, you can install it from Gitlab:
```R
devtools::install_gitlab("sportdatavalley/sdvclient-r")
```

When you have previously installed the library and want to upgrade to a newer version:
```R
remove.packages("sdvclient")
devtools::install_gitlab("sportdatavalley/sdvclient-r")
```

## Usage
```R
library(sdvclient)

for ( summary in sdvclient:myDatasets() ) {
    # Do something
}
```

The dataset summaries (which are [S4 classes](https://www.rdocumentation.org/packages/methods/versions/3.6.2/topics/Classes_Details)) that are returned from `sdvclient::myDatasets()` have slots like `title`, `event_start`, `event_end`, `owner`, `sport`, `tags` and more...

```R
summary@sport
>>> "sports.riding"
```

To retrieve data from your network:

```R
library(sdvclient)

for ( summary in sdvclient:networkDatasets() ) {
    # Do something
}
```

To retrieve data from a specific group in your network (see below for how to retrieve these groups):

```R
library(sdvclient)

for ( summary in sdvclient:groupDatasets(groupId = 1337) ) {
    # Do something
}
```

### Limit the number of results
`sdvclient::myDatasets()`, `sdvclient::networkDatasets()` and `sdvclient::groupDatasets()` accept an optional `limit` argument that can be used to limit the number of dataset summaries that are returned.

```R
library(sdvclient)

for ( summary in sdvclient:myDatasets(limit = 10) ) {
    # Do something
}
```

Please note that if there are less datasets available then the `limit` you specify, the number of returned dataset summaries is lower than `limit`.


### Retrieve groups and connections
To retrieve the groups in your network:

```R
library(sdvclient)

for ( group in sdvclient:groups() ) {
    # Do something
}
```

To retrieve the connections in your network: 
```R
library(sdvclient)

for ( connection in sdvclient:connections() ) {
    # Do something
}
```


### Retrieving raw/full data
After you have retrieved a dataset summary, you can then continue to download the raw/full data from this dataset by calling `sdvclient::getData()`:
```R
library(sdvclient)

for ( summary in sdvclient:myDatasets() ) {
    data <- getData(summary@id)
    # Do something
}
```

Or you can retrieve the raw/full data directly if you know the dataset id:
```R
library(sdvclient)

data <- getData(1337)
```


Every object that is returned from `sdvclient::getData()` has slots like `title`, `event_start`, `event_end`, `owner`, `sport`, `type`, `tags` and more fields depending on the data_type. For example a dataset with type `strava_type` has an attribute `dataframe` that contains a [data.frame](https://www.rdocumentation.org/packages/base/versions/3.6.2/topics/data.frame) with the data from this dataset.
```R
dataset@type
>>> "strava_type"
dataset@dataframe
>>> <data.frame>
```

#### Strava data type
As mentioned above, datasets of type `strava_type` have a slot dataframe with the corresponding data in a dataframe:
```R
dataset@type
>>> "strava_type"
dataset@dataframe
>>> <data.frame>
```

#### Questionnaire data type
Datasets of type questionnaire have a `questions` slot which contains of all the questions and answers in the questionnaire.
For each question+answer, the question and answer are available on the `question` and `answer` slots, respectivily.
```R
dataset@questions[[2]]@question
>>> "this is a question"

dataset@questions[[2]]@answer
>>> "this is an answer"
```

#### Generic CSV data type
For generic tabular data like csv's the returned dataset has a slot dataframe with the corresponding data in a dataframe:
```R
dataset@type
>>> "generic_csv_type"
dataset@dataframe
>>> <data.frame>
```

#### Daily activity data type
For daily activity data that is coming from e.g. Fitbit or Polar, the returned dataset has a range of slots:

- steps
- distance
- calories
- floors
- sleep_start
- sleep_end
- sleep_duration
- resting_heart_rate
- minutes_sedentary
- minutes_lightly_active
- minutes_fairly_active
- minutes_very_active

```R
dataset@type
>>> "fitbit_type"
dataset@resting_heart_rate
>>> 58
```

Please note that not all slots are always available, this is platform and device dependent.
E.g. Fitbit devices collect "minutes_sedentary", Polar devices do not.


#### Unstructured data
Unstructured data is data (files) that Sport Data Valley does not know how to process.
These files are stored "as is" in the platform and can be download via this client library as well:
For generic tabular data like csv's the returned dataset has a slot `dataframe` with the corresponding data in a dataframe:
Unstructured data has a `file_response` slot that contains a [requests.Response](https://requests.readthedocs.io/en/latest/api/#requests.Response) object.

```R
dataset@type
>>> "unstructured"
dataset@file_response
>>> <Response [200]>
```

Read more about processing these responses [here](https://cran.r-project.org/web/packages/httr/vignettes/quickstart.html).


#### Other data types
Although this library will be updated when new data types are added it can happen that a specific data type is not fully supported yet. In that case the returned dateset will be identical as unstructured data, with an `file_response` slot that contains an httr response objects.

```R
dataset@type
>>> "some new data type"
dataset@file_response
>>> <Response [200]>
```


### Authentication
The library retrieves your API token from the `SDV_AUTH_TOKEN` environment variable.
If you are working in the Sport Data Valley JupyterHub, this is automatically set.
If you are working in a different environment, you can retrieve an API token from the "Advanced" page [here](https://app.sportdatavalley.nl/profile/edit) and set it like this:

```R
sdvclient::setToken("your API token here")
```

Additionally, you can retrieve the token like this:
```R
sdvclient::getToken()
```



## Development

## Contributors
- [Aart Goossens](https://twitter.com/aartgoossens)

## License
See [LICENSE](LICENSE) file.
