getDatasetSummaries <- function(filter, groupId, limit) {
    if (missing(limit)) {
        limit <- 100
    }

    if ( filter == "my" ) {
        url <- "/timeline/my_metadata"
    } else if ( filter == "network" ) {
        url <- "/timeline/network_metadata"
    } else if ( filter == "group" ) {
        url <- sprintf("/groups/%s/recent_activity", groupId)
    }

    results = c()
    page <- 1
    url <- buildUrl(url)
    authHeader <- getAuthHeader()
    numberOfDatasets <- 1

    while (length(results) < limit && numberOfDatasets > 0) {
        response <- httr::GET(
            url = url,
            query = list("page" = page),
            httr::add_headers(Authorization = authHeader)
        )
        httr::warn_for_status(response)

        json <- httr::content(response, as = "text")
        responseData <- jsonlite::fromJSON(json, simplifyVector = FALSE)
        datasets <- responseData$data

        numberOfDatasets <- length(datasets)

        for (dataset in datasets) {
            if ( filter == "network" || filter == "group" ) {
                dataset <- dataset$metadatum
            }
            owner <- dataset$owner
            user <- sdvclient::User(
                id = owner$id,
                first_name = owner$first_name,
                last_name = owner$last_name
            )
            summary <- sdvclient::DatasetSummary(
                id = dataset$versioned_data_object_id,
                title = if ( is.null(dataset$title) ) "" else dataset$title,
                event_start = as.Date(dataset$event_start),
                event_end = as.Date(dataset$event_end),
                owner = user,
                sport = if ( is.null(dataset$sport$name ) ) "" else dataset$sport$name,
                tags = dataset$tags
            )
        }

        results <- append(results, c(summary))

        page <- page + 1

    }

    return(head(results, limit))
}


#' @export
myDatasets <- function(limit) {
    results <- getDatasetSummaries(limit = limit, filter = "my")

    return(results)
}


#' @export
networkDatasets <- function(limit) {
    results <- getDatasetSummaries(limit = limit, filter = "network")
    return(results)
}


#' @export
groupDatasets <- function(limit, groupId) {
    groupId  # To force an error when not provided

    results <- getDatasetSummaries(limit = limit, filter = "group", groupId = groupId)
    return(results)
}
